package user

import (
	"context"
	"log"

	"github.com/abigiya/internal/internal/module/util"
	"github.com/abigiya/internal/internal/storage/persistance/sqlc"
)

type UserData struct {
	Query *sqlc.Queries
}

func (us *UserData) User(ctx context.Context, user sqlc.UserStore) {
	hash, err := util.HashPassword(user.Password)
	if err != nil {
		log.Fatalf("unable to hash pasword %v", err)
	}

	us.Query.CreateUserStore(ctx, sqlc.CreateUserStoreParams{
		Firstname:    user.Firstname,
		Lastname:     user.Password,
		Emailaddress: user.Emailaddress,
		Username:     user.Username,
		Password:     hash,
	})
	ctx.Done()
}

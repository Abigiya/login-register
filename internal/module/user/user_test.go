package user_test

// import (
// 	"context"
// 	"database/sql"
// 	"testing"

// 	"github.com/abigiya/internal/internal/module/util"
// 	"github.com/abigiya/internal/internal/storage/persistance/sqlc"
// 	"github.com/stretchr/testify/require"
// )

// var (
// 	userTest    user.StoreAccess
// 	testQueries sqlc.Queries
// )

// var user = sqlc.CreateAccountParams{
// 	Firstname:    util.RandomName(),
// 	Lastname:     util.RandomName(),
// 	Emailaddress: util.RandomEmail(),
// 	Username:     util.RandomUserName(),
// 	Password:     string(util.RandomPassword()),
// }

// func TestCheckUserNameFormatAndExist(t *testing.T) {
// 	userTest = user.StoreAccess{
// 		DbPort: &sqlc.SQLAdaptor{
// 			Queries: testingQueries,
// 		},
// 		rawun: &sqlc.Account{},
// 	}
// 	t.Run("username incorrect format", func(t *testing.T) {
// 		new, _, _ := userTest.CheckUserNameFormatAndExist(util.RandomUserNameIncorrectFormat())
// 		acc, err := testQueries.GetAccountByUsername(context.Background(), user.Username)

// 		require.Equal(t, acc.Username, new.Username)
// 		require.Empty(t, err)
// 	})

// 	t.Run("user name exist", func(t *testing.T) {
// 		new, _, _ := userTest.CheckUserNameFormatAndExist(util.RandomUserName())
// 		acc, err := testQueries.CreateAccount(context.Background(), user)

// 		require.Equal(t, acc, new)
// 		require.EqualError(t, err, sql.ErrNoRows.Error())
// 		require.Empty(t, err)
// 	})
// }

// func TestCheckProperInput(t *testing.T) {
// 	t.Run("Invalid email address format", func(t *testing.T) {
// 		crct, _ := userTest.ConfirmAccount(context.Background(), util.RandomName(), util.RandomName(), util.RandomEmail(), util.RandomUserName(), string(util.RandomPassword()))
// 		err := userTest.CheckProperInput(
// 			util.RandomName(),
// 			util.RandomName(),
// 			util.RandomInvalidEmailtype1(),
// 			util.RandomUserName(),
// 			string(util.RandomPassword()))
// 		require.Equal(t, crct, err)
// 	})

// 	t.Run("Invalid email address format", func(t *testing.T) {
// 		crct, _ := userTest.ConfirmAccount(context.Background(), util.RandomName(), util.RandomName(), util.RandomEmail(), util.RandomUserName(), string(util.RandomPassword()))
// 		err := userTest.CheckProperInput(
// 			util.RandomName(),
// 			util.RandomName(),
// 			util.RandomInvalidEmailtype2(),
// 			util.RandomUserName(),
// 			string(util.RandomPassword()))
// 		require.Equal(t, crct, err)
// 	})
// }

// func TestConfirmAccount(t *testing.T) {
// 	t.Run("confirm Account", func(t *testing.T) {
// 		expected, _ := userTest.ConfirmAccount(context.Background(), util.RandomName(), util.RandomName(), util.RandomEmail(), util.RandomUserName(), string(util.RandomPassword()))
// 		actual := userTest.CheckProperInput(
// 			util.RandomName(),
// 			util.RandomName(),
// 			util.RandomEmail(),
// 			util.RandomUserName(),
// 			string(util.RandomPassword()))
// 		require.Equal(t, expected, actual)
// 	})
// }

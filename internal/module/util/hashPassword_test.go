package util

import (
	"fmt"
	"log"
	"testing"
)

func TestHashPassword(t *testing.T) {
	password := "secret"
	hash, err := HashPassword(password)
	if err != nil {
		log.Fatalf("cannot hash password %v", err)
	}

	fmt.Println("Password:", password)
	fmt.Println("Hash:    ", hash)

	match := CheckPassword(password, hash)
	fmt.Println("Match:   ", match)
}

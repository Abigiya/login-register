package util

import (
	"math/rand"
	"strings"
	"time"
)

const (
	alphabet     = "@#$&*!abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	followedByAt = "@"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func RandomInt(min, max int64) int64 {
	return min + rand.Int63n(max-min+1)
}

func RandomString(n int) string {
	var sb strings.Builder
	k := len(alphabet)

	for i := 0; i < n; i++ {
		c := alphabet[rand.Intn(k)]
		sb.WriteByte(c)
	}
	return sb.String()
}

func RandomName() string {
	return RandomString(4)
}

func RandomEmail() string {
	return RandomString(5) + followedByAt + "gmail.com"
}

func RandomInvalidEmailtype1() string {
	return RandomString(5) + followedByAt
}

func RandomInvalidEmailtype2() string {
	return RandomString(5) + "gmail.com"
}

func RandomUserName() string {
	return followedByAt + RandomString(5)
}

func RandomUserNameIncorrectFormat() string {
	return RandomString(5)
}

func RandomPassword() string {
	return RandomString(8)
}

// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.15.0
// source: query2.sql

package sqlc

import (
	"context"
)

const createUserStore = `-- name: CreateUserStore :one
INSERT INTO user_store (
    firstname, lastname, emailaddress, username, password
)
VALUES(
    $1, $2, $3, $4, $5
) RETURNING id, firstname, lastname, emailaddress, username, password, created_at
`

type CreateUserStoreParams struct {
	Firstname    string `json:"firstname"`
	Lastname     string `json:"lastname"`
	Emailaddress string `json:"emailaddress"`
	Username     string `json:"username"`
	Password     string `json:"password"`
}

func (q *Queries) CreateUserStore(ctx context.Context, arg CreateUserStoreParams) (UserStore, error) {
	row := q.queryRow(ctx, q.createUserStoreStmt, createUserStore,
		arg.Firstname,
		arg.Lastname,
		arg.Emailaddress,
		arg.Username,
		arg.Password,
	)
	var i UserStore
	err := row.Scan(
		&i.ID,
		&i.Firstname,
		&i.Lastname,
		&i.Emailaddress,
		&i.Username,
		&i.Password,
		&i.CreatedAt,
	)
	return i, err
}

const getUserStore = `-- name: GetUserStore :one
SELECT id, firstname, lastname, emailaddress, username, password, created_at FROM user_store
WHERE username= $1 
AND password= $2 LIMIT 1
`

type GetUserStoreParams struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (q *Queries) GetUserStore(ctx context.Context, arg GetUserStoreParams) (UserStore, error) {
	row := q.queryRow(ctx, q.getUserStoreStmt, getUserStore, arg.Username, arg.Password)
	var i UserStore
	err := row.Scan(
		&i.ID,
		&i.Firstname,
		&i.Lastname,
		&i.Emailaddress,
		&i.Username,
		&i.Password,
		&i.CreatedAt,
	)
	return i, err
}

const getUserStoreByUsername = `-- name: GetUserStoreByUsername :one
SELECT id, firstname, lastname, emailaddress, username, password, created_at FROM user_store
WHERE username= $1 LIMIT 1
`

func (q *Queries) GetUserStoreByUsername(ctx context.Context, username string) (UserStore, error) {
	row := q.queryRow(ctx, q.getUserStoreByUsernameStmt, getUserStoreByUsername, username)
	var i UserStore
	err := row.Scan(
		&i.ID,
		&i.Firstname,
		&i.Lastname,
		&i.Emailaddress,
		&i.Username,
		&i.Password,
		&i.CreatedAt,
	)
	return i, err
}

package sqlc_test

import (
	"context"
	"database/sql"
	"testing"

	"github.com/abigiya/internal/internal/module/util"
	"github.com/abigiya/internal/internal/storage/persistance/sqlc"
	"github.com/stretchr/testify/require"
)

func CreateRandomUserStore(t *testing.T) sqlc.UserStore {
	arg := sqlc.CreateUserStoreParams{
		Firstname:    util.RandomName(),
		Lastname:     util.RandomName(),
		Emailaddress: util.RandomEmail(),
		Username:     util.RandomUserName(),
		Password:     string(util.RandomPassword()),
	}

	acc, err := testQueries.CreateUserStore(context.Background(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, acc)

	require.Equal(t, acc.Firstname, arg.Firstname)
	require.Equal(t, acc.Lastname, arg.Lastname)
	require.Equal(t, acc.Emailaddress, arg.Emailaddress)
	require.Equal(t, acc.Username, arg.Username)
	require.Equal(t, acc.Password, arg.Password)

	require.NotZero(t, acc.ID)
	require.NotZero(t, acc.CreatedAt)

	return acc
}

func TestCreateUserStore(t *testing.T) {
	CreateRandomUserStore(t)
}

func TestGetUserStore(t *testing.T) {
	account1 := CreateRandomUserStore(t)
	t.Run("Fill username and password", func(t *testing.T) {
		followAccountOnly, err := testQueries.GetUserStore(context.Background(), sqlc.GetUserStoreParams{
			Username: account1.Username,
			Password: string(account1.Password),
		})
		require.NoError(t, err)
		require.NotEmpty(t, followAccountOnly)
		require.Equal(t, account1.ID, followAccountOnly.ID)
		require.Equal(t, account1.Username, followAccountOnly.Username)
		require.Equal(t, account1.Password, followAccountOnly.Password)
		require.Equal(t, account1.CreatedAt, followAccountOnly.CreatedAt)
	})

	t.Run("user name not exit", func(t *testing.T) {
		followUsernameFormat, err := testQueries.GetUserStore(context.Background(), sqlc.GetUserStoreParams{
			Username: util.RandomUserName(),
			Password: string(util.RandomPassword()),
		})
		require.EqualError(t, err, sql.ErrNoRows.Error())
		require.Empty(t, followUsernameFormat)
	})
}

CREATE TABLE IF NOT EXISTS user_store (
  id UUID NOT NULL DEFAULT gen_random_uuid(),
  firstname varchar NOT NULL,
  lastname varchar NOT NULL,
  emailaddress varchar UNIQUE NOT NULL,
  username varchar UNIQUE NOT NULL,
  password varchar NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now()
);

-- name: GetUserStore :one
SELECT * FROM user_store
WHERE username= $1 
AND password= $2 LIMIT 1;

-- name: GetUserStoreByUsername :one 
SELECT * FROM user_store
WHERE username= $1 LIMIT 1;

-- name: CreateUserStore :one
INSERT INTO user_store (
    firstname, lastname, emailaddress, username, password
)
VALUES(
    $1, $2, $3, $4, $5
) RETURNING *;

package model

import (
	"github.com/abigiya/internal/internal/storage/persistance/sqlc"
)

type UserService interface {
	Validate() (sqlc.UserStore, error)
}

type UserServiceCatch struct {
	UserService UserService
	ValidUser   *User
}

// type DbPort interface {
// 	Close()
// 	GetaAccountParams(username string, password string) sqlc.GetAccountParams
// 	AccountStructure() sqlc.Account
// 	CreateParams(newAcc sqlc.CreateAccountParams) sqlc.Account
// 	Query() sqlc.Queries
// }

type UserServiceAdaptor interface {
	Register(create *sqlc.CreateUserStoreParams) ResponseAcceptor
}

type ResponseAcceptor struct {
	Store  sqlc.UserStore
	Status string
}

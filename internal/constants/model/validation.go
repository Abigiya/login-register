package model

import (
	"regexp"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

type User struct {
	Firstname    string `json:"firstname"`
	Lastname     string `json:"lastname"`
	Emailaddress string `json:"emailaddress"`
	Username     string `json:"username"`
	Password     string `json:"password"`
}

// validate user input and return error message and create account parameters from the sqlc package
func (user User) Validate() error {
	return validation.ValidateStruct(&user,
		validation.Field(&user.Firstname, validation.Required.Error("empty first name")),
		validation.Field(&user.Lastname, validation.Required.Error("empty last name")),
		validation.Field(&user.Emailaddress, validation.Required.Error("empty email"), is.Email),
		validation.Field(&user.Username, validation.Match(regexp.MustCompile("^@[a-zA-Z]+$"))),
		validation.Field(&user.Password, validation.Required.Error("empty password")),
	)
}

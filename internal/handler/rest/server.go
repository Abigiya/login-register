package handler

import (
	"database/sql"
	"log"

	"github.com/abigiya/internal/internal/storage/persistance/sqlc"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

// ServerAdapter serves all http requests of register
type ServerAdaptor struct {
	router *gin.Engine
}

var Query *sqlc.Queries

// NewAdaptor serve http request and sent to database store
func NewDbAdaptor(dbDriver, dbSource string) ServerAdaptor {
	conn, err := sql.Open(dbDriver, dbSource)

	if err != nil {
		log.Fatal("can not connect to database", err)
	}

	Query = sqlc.New(conn)

	serveAdaptor := ServerAdaptor{
		router: &gin.Engine{},
	}

	router := gin.New()
	router.POST("/Register", serveAdaptor.SignUp)
	serveAdaptor.router = router

	return serveAdaptor
}

// start a request server with a specific address
func (serve *ServerAdaptor) RequestSendStart(address string) error {
	log.Print("Callaing Server")
	run := serve.router.Run(address)
	return run
}

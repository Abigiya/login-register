package handler

import (
	"fmt"
	"log"
	"net/http"

	"github.com/abigiya/internal/internal/constants/model"
	"github.com/abigiya/internal/internal/module/util"
	"github.com/abigiya/internal/internal/storage/persistance/sqlc"
	"github.com/gin-gonic/gin"
	validation "github.com/go-ozzo/ozzo-validation"
	_ "github.com/lib/pq"
)

func (serve *ServerAdaptor) SignUp(ctx *gin.Context) {

	var user = model.User{}
	if err := ctx.ShouldBindJSON(&user); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}
	log.Printf("Data REQUEST:%v", user)

	err := user.Validate()
	log.Printf("validation err:%v", err)

	if err != nil {
		if e, ok := err.(validation.InternalError); ok {
			fmt.Println("Internal validation error happened: ", e.InternalError())
			return
		}
		ctx.JSON(http.StatusBadRequest, gin.H{
			"request errors: ": err,
		})
		return
	}

	hash, err := util.HashPassword(user.Password)
	if err != nil {
		log.Fatalf("unable to hash pasword %v", err)
	}

	respond, err := Query.CreateUserStore(ctx, sqlc.CreateUserStoreParams{
		Firstname:    user.Firstname,
		Lastname:     user.Lastname,
		Emailaddress: user.Emailaddress,
		Username:     user.Username,
		Password:     hash,
	})
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("REQUEST DONE")

	ctx.JSON(http.StatusAccepted, gin.H{
		"Registered!": respond,
	})
}

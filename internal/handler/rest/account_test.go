package handler

import (
	"testing"

	"github.com/abigiya/internal/internal/module/util"
	"github.com/abigiya/internal/internal/storage/persistance/sqlc"
	"github.com/gin-gonic/gin"
)

var user = sqlc.CreateUserStoreParams{
	Firstname:    util.RandomName(),
	Lastname:     util.RandomName(),
	Emailaddress: util.RandomEmail(),
	Username:     util.RandomUserName(),
	Password:     string(util.RandomPassword()),
}

func TestSignUp(t *testing.T) {
	gin.SetMode(gin.TestMode)

	t.Run("unable to bind data", func(t *testing.T) {
		
	})

	t.Run("Invalid username format", func(t *testing.T) {

	})

	t.Run("Invalid email", func(t *testing.T) {

	})

	t.Run("sucessful registration", func(t *testing.T) {

	})
}

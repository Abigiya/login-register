package handler_test

import (
	"database/sql"
	"log"
	"os"
	"testing"

	"github.com/abigiya/internal/initiator"
	"github.com/abigiya/internal/internal/storage/persistance/sqlc"
	_ "github.com/lib/pq"
)

var testQuery *sqlc.Queries

func TestMain(m *testing.M) {
	config, err := initiator.LoadConfig("../../..")
	if err != nil {
		log.Fatal("can not configure file:", err)
	}

	conn, err := sql.Open(config.DBDriver, config.DBSource)
	if err != nil {
		log.Fatalf("cannot import from the data base %v", err)
	}
	testQuery = sqlc.New(conn)

	os.Exit(m.Run())
}

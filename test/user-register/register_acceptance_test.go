package userregister

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"

	"github.com/abigiya/internal/initiator"
	handler "github.com/abigiya/internal/internal/handler/rest"
	"github.com/cucumber/godog"
)

type Response struct {
	//suite    *godog.ScenarioContext
	response string
}

var (
	TestUsername     []string
	TestEmail        []string
	newServerAdapter *handler.ServerAdaptor
	res              *Response
)

type UserData struct {
	Firstname    string
	Lastname     string
	Emailaddress string
	Username     string
	Password     string
}

func init() {
	config, err := initiator.LoadConfig("../../")
	if err != nil {
		log.Fatal("can not configure an env file", err)
	}
	newServerAdapter = handler.NewDbAdaptor(config.DBDriver, config.DBSource)
}

func iAmNewUser() error {
	return nil
}

// define conditions each i/p become nil, inject data directly to test db
func (res *Response) iRegisterWithInformationOf(Firstname, Lastname, Emailaddress, Username, Password string) (UserData, error) {

	return UserData{}, nil
}

func (res *Response) iRegisterWithEmptyInformationOf(firstname, lastname, emailaddress, username, password string) error {
	data := map[string]string{"firstname": firstname, "lastname": lastname, "emailaddress": emailaddress, "username": username, "password": password}

	json_data, err := json.Marshal(data)
	if err != nil {
		return err
	}
	resp, err := http.Post("0.0.0.0:8181/Register", "application/json", bytes.NewBuffer(json_data))
	if err != nil {
		return err
	}
	var re map[string]string

	json.NewDecoder(resp.Body).Decode(&res)

	res.response = re["error"]
	return nil
}

func (res *Response) haveListOfUser(arg1 *godog.Table) error {

	return nil
}

func (res *Response) iShouldGetAResponseOf(arg1 string) error {
	return godog.ErrPending
}

func (res *Response) iShouldGetTheResponseOf(arg1 string) error {
	return godog.ErrPending
}

func (res *Response) resultShouldBe(arg1 string) error {
	return godog.ErrPending
}

func (res *Response) theResponseShouldBe(arg1 string) error {
	return godog.ErrPending
}

func (res *Response) userFillAnInformationOf(arg1, arg2 string) error {
	return godog.ErrPending
}

func (res *Response) userFillInformationOf(arg1, arg2, arg3 string) error {
	return godog.ErrPending
}

func InitializeScenario(ctx *godog.ScenarioContext) {
	ctx.Step(`^Have List of User$`, res.haveListOfUser)
	ctx.Step(`^I am New User$`, iAmNewUser)
	ctx.Step(`^I register with empty information of "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)"$`, res.iRegisterWithEmptyInformationOf)
	ctx.Step(`^I register with information of "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)", "([^"]*)"$`, res.iRegisterWithInformationOf)
	ctx.Step(`^I should get a response of "([^"]*)"$`, res.iShouldGetAResponseOf)
	ctx.Step(`^I should get the response of "([^"]*)"$`, res.iShouldGetTheResponseOf)
	ctx.Step(`^result should be "([^"]*)"$`, res.resultShouldBe)
	ctx.Step(`^The response should be "([^"]*)"$`, res.theResponseShouldBe)
	ctx.Step(`^User fill an information of "([^"]*)", "([^"]*)"$`, res.userFillAnInformationOf)
	ctx.Step(`^User fill information of "([^"]*)", "([^"]*)", "([^"]*)"$`, res.userFillInformationOf)
}

@Scenarios @api
Feature: Register

  # @failrequest
  # Scenario Outline: Does not allow the request
  # Given I am New user
  # When I send "POST" a request to "/Register"
  # Then I should get a status code of 405

  # @sucessrequest
  # Scenario Outline: Request allowed
  # Given I am New user
  # When I send "POST" a request to "/Register"
  # Then I should get a status code of 200

  @empty
  Scenario Outline: Empty Firstname, Lastname, Emailaddress, Username, Password
    Given I am New user
    When I register with empty information of "<Firstname>", "<Lastname>", "<Emailaddress>", "<Username>", "<Password>"
    Then I should get the response of "<vaidation result>"

  Example:
      | Firstname   | Lastname    | Emailaddress    | Username | password | validation result |
      | " "         | pass        | pass            | pass     | pass     | empty first name  |
      | user2 FName | " "         | pass            | pass     | pass     | empty last name   |
      | user3 FName | user2 LName | " "             | pass     | pass     | empty email       |
      | user4 FName | user3 LName | user3@gmail.com | " "      | pass     | empty user name   |
      | user5 FName | user4 LName | user4@yahoo.com | @user4   | " "      | empty Password    |

  @invalid
  Scenario Outline: Invalid form of email address, user name and password
    Given I am New User
    When I register with information of "<First name>", "<Last name>", "<email address>", "<User name>", "<Password>"
    Then I should get a response of "<validation result>"
  Example:
      | First Name  | Last Name   | Email Address   | User Name  | Password               | validation result       |
      | user1 FName | user1 LName | user1@mail      | @username1 | abig@ABIG452           | invalid email           |
      | user2 FName | user2 LName | user2mail       | @username2 | 6abg$ABIG452           | invalid email           |
      | user3 FName | user3 LName | user3@gmail.com | user       | abi27#ABIG52           | invalid username format |
      | user4 FName | user4 LName | user4@gmail.com | @username4 | 23@abi                 | short password          |
      | user5 FName | user5 LName | user5@yahoo.com | @username5 | abigiya@ABIGIYA1234567 | over limit password     |

  @existemail
  Scenario Outline: Email exist
    Given Have List of User
      | Email Address       | User Name  | Password |
      | user@gmail.com      | @username1 | #abiABI  |
      | existuser@gmail.com | @username2 | use@USER |
    When User fill information of "<email address>", "<user name>", "<password>"
    Then The response should be "<validation result>"
  Example:
      | Email Address       | User Name  | Password | validation result |
      | user@gmail.com      | @username1 | #abiABI  | valid             |
      | existuser@gmail.com | @username2 | #abiABI  | email exist       |

  @existusername
  Scenario Outline: User name exist
    Given Have List of User
      | User Name      | password   | Email Address   |
      | @username      | #abiABI454 | user1@gmail.com |
      | @existusername | use@User67 | user2@gmaol.com |
    When User fill an information of "<User name>", "<password>"
    Then result should be "<validation result>"
  Example:
      | User Name      | Password   | validation result |
      | @username      | #abiABI454 | valid             |
      | @existusername | use@User67 | username exist    |

  @registered
  Scenario Outline: Sucessful registration
    Given I am New User
    When I register with information of "<First name>", "<Last name>", "<email address>", "<User name>", "<Password>"
    Then I should get a response of "<Registeration sucessful>"

  Example:
      | First Name  | Last Name   | Email Address   | User Name | Password     | validation result  |
      | user1 FName | user1 LName | user1@gmail.com | @user1    | 6abg$ABIG452 | register sucessful |
      | user2 FName | user2 LName | user2@gmail.com | @user2    | abig@ABIG452 | register sucessful |

package main

import (
	"github.com/abigiya/internal/initiator"
)

func main() {
	initiator.ServerInit()
}

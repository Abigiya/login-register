package initiator

import (
	"log"

	handler "github.com/abigiya/internal/internal/handler/rest"
	_ "github.com/lib/pq"
)

func ServerInit() {
	config, err := LoadConfig("./")
	if err != nil {
		log.Fatal("cannot configure with the file:", err)
	}

	server := handler.NewDbAdaptor(config.DBDriver, config.DBSource)
	server.RequestSendStart(config.ServerAddress)
}
